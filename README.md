# Тестовое задание МТС

## Как запускать


#### Использованные материалы:
1. https://medium.com/southbridge/prometheus-monitoring-ba8fbda6e83
1. https://github.com/kjanshair/docker-prometheus
1. https://prometheus.io/docs/prometheus/latest/querying/api/
1. https://prometheus.io/docs/guides/node-exporter/
1. https://habr.com/ru/company/southbridge/blog/455290/
1. https://youtu.be/-yCtBUtTD9o
1. https://pypi.org/project/prometheus-flask-exporter/
1. https://habr.com/ru/post/518122/
1. https://github.com/rycus86/prometheus_flask_exporter/tree/master/examples/sample-signals